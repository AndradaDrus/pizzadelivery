//
//  AsyncBlockOperation.h
//  PizzaDelivery
//
//  Created by user on 07/04/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKMaps/SKMaps.h"

typedef void(^AsyncBlock)(SKRouteSettings *routeSettings);
@interface AsyncBlockOperation : NSOperation <SKRoutingDelegate>

//@property (nonatomic, readonly, copy) AsyncBlock routingSettings;

@property NSNumber *dist;
@property SKRouteSettings *routeSettings;

- (instancetype)initWithRoute:(SKRouteSettings *)routeSettings; //(AsyncBlock)routingSettings;
- (void)didfinishcalculationwithinfo:(NSDictionary *)dioct;
@end
