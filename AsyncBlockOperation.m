//
//  AsyncBlockOperation.m
//  PizzaDelivery
//
//  Created by user on 07/04/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "AsyncBlockOperation.h"

@interface AsyncBlockOperation ()

//@property (nonatomic, copy) AsyncBlock routingSettings;
@property  BOOL finished;
@property BOOL executing;
@property SKRoutingService *rs;
@property SKRouteInformation *routeInformation;

@property NSInteger distNr;

@end


@implementation AsyncBlockOperation
@synthesize finished;
@synthesize executing;


//:(AsyncBlock)block
- (instancetype)initWithRoute:(SKRouteSettings *)routeSettings {
    if (self = [super init]) {
        self.routeSettings = routeSettings;
        
        
        self.distNr++;
    }
    return self;
}

- (void)start {
    [self willChangeValueForKey:@"isExecuting"];
    self.executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    self.finished = NO;
    [self didChangeValueForKey:@"isFinished"];
    [SKRoutingService sharedInstance].routingDelegate = self;
    [[SKRoutingService sharedInstance]calculateRoute:self.routeSettings];
    
}

- (BOOL)isFinished{
    return self.finished;
}

- (BOOL)isExecuting{
    return self.executing;
}

- (BOOL)isAsynchronous{
    return YES;
}


- (void)routingService:(SKRoutingService*)routingService didFinishRouteCalculationWithInfo:(SKRouteInformation*)routeInformation{
    [routingService zoomToRouteWithInsets:UIEdgeInsetsZero duration:1];
    self.dist = [NSNumber numberWithInt:routeInformation.distance];
}

- (void)routingServiceDidCalculateAllRoutes:(SKRoutingService *)routingService {
    [self willChangeValueForKey:@"isExecuting"];
    self.executing = NO;
    [self didChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    self.finished = YES;
    [self didChangeValueForKey:@"isFinished"];

}

- (void)routingService:(SKRoutingService *)routingService didFailWithErrorCode:(SKRoutingErrorCode)errorCode {
    [self willChangeValueForKey:@"isExecuting"];
    self.executing = NO;
    [self didChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    self.finished = YES;
    [self didChangeValueForKey:@"isFinished"];

}



@end
