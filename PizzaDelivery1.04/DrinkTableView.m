//
//  DrinkTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "DrinkTableView.h"
#import "Drink.h"

@interface DrinkTableView()
@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;
@property (strong, nonatomic) NSDictionary *drinkDictionary;
@property (strong, nonatomic) NSDictionary *drinkIdDictionary;
@property NSInteger cnt;
@property (strong, nonatomic) NSMutableArray *drinkList;

@property (strong, nonatomic) Drink *d;
@end

NSString *jsonwebstr = @"{\"DrinkResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";


@implementation DrinkTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
     [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
     [pizzaReq setHTTPMethod:@"GET"];
     
     
     NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
     
     [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
     NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
     
     NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
     self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
     
     */
    self.cnt = 0;
    
    self.data = jsonwebstr;
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];
    
    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"DrinkResult"];    //arr[arr]
    self.drinkList = [NSMutableArray array];
    
    
    for(int i = 0; i < [self.arr count]; i++){
        self.d = [[Drink alloc] init];
        self.cnt++;
        self.d.did = [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"];
        self.d.name = [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"];
        /*  self.d.descr = [[self.arr objectAtIndex:i] objectAtIndex:2][@"Value"];
         self.d.price = [[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"];
         self.d.gr = [[self.arr objectAtIndex:i] objectAtIndex:3][@"Value"];
         [self.drinkList addObject:[[[[[[self.d.name stringByAppendingString:@" "]
         stringByAppendingString:self.d.gr]
         stringByAppendingString:@" g "]
         stringByAppendingString:self.d.price]
         stringByAppendingString:@" lei \n"]
         stringByAppendingString:self.d.descr]];
         */
        
        [self.drinkList addObject: [self.d.name stringByAppendingString:self.d.name]];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return self.cnt;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Drinks";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.textLabel.text = [self.drinkList objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    self.drinkDictionary = [NSDictionary dictionaryWithObject: [self.drinkList objectAtIndex: indexPath.row ] forKey:@1];
    self.drinkIdDictionary = [NSDictionary dictionaryWithObject: @(indexPath.row + 1) forKey:@1];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DrinkAdded" object:self userInfo:self.drinkDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"DrinkAddedId" object:self userInfo:self.drinkIdDictionary];
}

@end
