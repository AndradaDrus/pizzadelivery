//
//  SauceTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "SauceTableView.h"
#import "Sauce.h"

@interface SauceTableView()
@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;
@property (strong, nonatomic) NSDictionary *sauceDictionary;
@property (strong, nonatomic) NSDictionary *sauceIdDictionary;
@property NSInteger cnt;
@property (strong, nonatomic) NSMutableArray *sauceList;

@property (strong, nonatomic) Sauce *s;

@end

NSString *jsonwebstri = @"{\"SauceResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";

@implementation SauceTableView
- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
     [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
     [pizzaReq setHTTPMethod:@"GET"];
     
     
     NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
     
     [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
     NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
     
     NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
     self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
     
     */
    self.cnt = 0;
    
    self.data = jsonwebstri;
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];
    
    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"SauceResult"];    //arr[arr]
    self.sauceList = [NSMutableArray array];
    
    
    for(int i = 0; i < [self.arr count]; i++){
        self.s = [[Sauce alloc] init];
        self.cnt++;
        self.s.sid = [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"];
        self.s.name = [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"];
        /*
         self.s.price = [[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"];
         
         [self.sauceList addObject:[[[self.s.name stringByAppendingString:@" "]
         stringByAppendingString:self.s.price]
         stringByAppendingString:@" lei \n"]];
         */
        [self.sauceList addObject: [self.s.name stringByAppendingString:self.s.name]];
        
        
    }
    
        [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return self.cnt;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Sauces";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }

    cell.textLabel.text = [self.sauceList objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    self.sauceDictionary = [NSDictionary dictionaryWithObject: [self.sauceList objectAtIndex: indexPath.row ] forKey:@1];
    self.sauceIdDictionary = [NSDictionary dictionaryWithObject: @(indexPath.row + 1) forKey:@1];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SauceAdded" object:self userInfo:self.sauceDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"SauceAddedId" object:self userInfo:self.sauceIdDictionary];
    
}


@end
