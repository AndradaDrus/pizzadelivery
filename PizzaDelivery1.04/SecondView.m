//
//  SecondView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "SecondView.h"

@interface SecondView()

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) NSDictionary *u;
@property (strong, nonatomic) NSDictionary *p;
@property (strong, nonatomic) NSDictionary *a;
@end

@implementation SecondView

-(IBAction) btnClicked:(UIButton *)sender
{
    NSLog(@"Clicked");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Finish" object:self];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{

    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.u = [NSDictionary dictionaryWithObject:self.userName.text forKey:@1];
    if([self.phone.text length]==10)
        self.p = [NSDictionary dictionaryWithObject:[f numberFromString:self.phone.text] forKey:@1];
    else NSLog(@"Wrong phone number");//TODO:
    self.a = [NSDictionary dictionaryWithObject:self.address.text forKey:@1];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAdded" object:self userInfo:self.u];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PhoneAdded" object:self userInfo:self.p];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddressAdded" object:self userInfo:self.a];
    
    
    
    return YES;
}

@end