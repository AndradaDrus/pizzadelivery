//
//  AppDelegate.h
//  PizzaDelivery
//
//  Created by user on 23/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

