//
//  Drink.m
//  PizzaDelivery
//
//  Created by user on 29/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "Drink.h"

@interface Drink ()
@property (strong, nonatomic) NSNumber *did;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) NSNumber *gr;

@end

@implementation Drink

@end
