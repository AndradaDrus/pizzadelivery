//
//  DrinkTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "DrinkTableView.h"

@interface DrinkTableView()
@property (strong, nonatomic) NSString *jsonString;
@property (strong, nonatomic) NSArray *jsonArr;
@end

@implementation DrinkTableView

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return [self.jsonArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Drinks";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.jsonArr objectAtIndex:indexPath.row];
    return cell;
}

@end
