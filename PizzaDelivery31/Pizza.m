//
//  Pizza.m
//  PizzaDelivery
//
//  Created by user on 29/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "Pizza.h"

@interface Pizza ()
@property (strong, nonatomic) NSNumber *pid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSNumber *gr;
@property (strong, nonatomic) NSNumber *price;

@end

@implementation Pizza

- (void)viewDidLoad {
    self.name = [[NSString alloc] init];
}

- (void) didReceiveMemoryWarning {
    
}

- (void)setPid:(NSNumber *)pid{
    self.pid = pid;
}

- (void)setName:(NSString *)name{
    self.name = name;
}

- (void)setDescr:(NSString *)descr{
    self.descr = descr;
}

- (void)setGr:(NSNumber *)gr{
    self.gr = gr;
}

- (void)setPrice:(NSNumber *)price{
    self.price = price;
}

- (NSNumber *)getPid {
    return self.pid;
}

- (NSString *)getName {
    return  self.name;
}

- (NSString *)getDescr {
    return self.descr;
}

- (NSNumber *)getPrice{
    return self.price;
}

- (NSNumber *)getGr{
    return self.gr;
}

@end
