//
//  PizzaTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "PizzaTableView.h"

@interface PizzaTableView()

@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;
@end

@implementation PizzaTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
    [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
    [pizzaReq setHTTPMethod:@"GET"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
        NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
   
        NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
    self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
    
      */
   
    self.data = @"{\"PizzaResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";
    
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];

    self.arr = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
  /*  self.jsonDict = [self.arr objectAtIndex:1];
    NSString *test = [self.jsonDict objectForKey:@"ID"];
    NSLog(@"Test is %@",test);

       */
   [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arr count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.arr objectAtIndex:indexPath.row];
    return cell;
}

@end
