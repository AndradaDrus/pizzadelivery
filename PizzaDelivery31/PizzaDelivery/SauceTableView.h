//
//  SauceTableView.h
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SauceTableView : UITableViewController <UITableViewDelegate,UITableViewDataSource>

@end
