//
//  PizzaTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "PizzaTableView.h"


@interface PizzaTableView()

@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;

@property (strong, nonatomic) NSMutableArray *pid;
@property (strong, nonatomic) NSMutableArray *pizzaList;


@end


NSString *jsonwebstring = @"{\"PizzaResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";

@implementation PizzaTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
    [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
    [pizzaReq setHTTPMethod:@"GET"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
        NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
   
        NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
    self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
    
      */
    
    self.data = jsonwebstring;
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];

    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"PizzaResult"];    //arr[arr]
    
    self.pid = [NSMutableArray array];
    NSMutableArray *name = [NSMutableArray array];
    NSMutableArray *descr = [NSMutableArray array];
    NSMutableArray *price = [NSMutableArray array];
    NSMutableArray *gr = [NSMutableArray array];
    

    
    for(int i = 0; i < [self.arr count]; i++){
         [self.pid addObject: [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"]];
         [name addObject: [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"] ];

    }
    
    for(int i = 0; i < [self.arr count]; i++){
       // Pizza *p = [[Pizza alloc] init];
       // [p setPid:[[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"]];
       // [p setName:[[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"]];
       // [p setDescr:[[self.arr objectAtIndex:i] objectAtIndex:2][@"Value"]];
        //[p setPrice:[[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"]];
        //[p setGr:[[self.arr objectAtIndex:i] objectAtIndex:3][@"Value"]];
        
    }

    
   [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return [self.pid count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pizza list";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.pid objectAtIndex:indexPath.row];

    return cell;
}

@end
