//
//  Sauce.m
//  PizzaDelivery
//
//  Created by user on 29/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "Sauce.h"

@interface Sauce ()
@property (strong, nonatomic) NSNumber *sid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *price;

@end

@implementation Sauce

@end
