//
//  DrinkTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "DrinkTableView.h"
#import "Drink.h"

@interface DrinkTableView()
@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;
@property (strong, nonatomic) NSDictionary *drinkDictionary;
@property (strong, nonatomic) NSDictionary *drinkIdDictionary;
@property NSInteger cnt;
@property (strong, nonatomic) NSMutableArray *drinkList;
@property (strong, nonatomic) NSMutableArray *coldDrinkList;

@property (strong, nonatomic) Drink *d;
@end

NSString *jsonwebstr = @"{\"DrinkResult\":[[{\"Key\":\"id\",\"Value\":13},{\"Key\":\"name\",\"Value\":\"Espresso\"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":7},{\"Key\":\"description\",\"Value\":\"\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":14},{\"Key\":\"name\",\"Value\":\"Espresso Machiatto\"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":7},{\"Key\":\"description\",\"Value\":\"cu lapte spumat\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":15},{\"Key\":\"name\",\"Value\":\"Cappuccino Vienez\"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":8},{\"Key\":\"description\",\"Value\":\"cu frisca\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":16},{\"Key\":\"name\",\"Value\":\"Caffe Latte\"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":9},{\"Key\":\"description\",\"Value\":\"\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":17},{\"Key\":\"name\",\"Value\":\"Ceai \"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":5.5},{\"Key\":\"description\",\"Value\":\"diverse sortimente\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":18},{\"Key\":\"name\",\"Value\":\"Ciocolata calda\"},{\"Key\":\"type\",\"Value\":\"bcald\"},{\"Key\":\"price\",\"Value\":11},{\"Key\":\"description\",\"Value\":\"Neagra\/ Alba\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":22},{\"Key\":\"name\",\"Value\":\"Martini Bianco \"},{\"Key\":\"type\",\"Value\":\"breci\"},{\"Key\":\"price\",\"Value\":8.9},{\"Key\":\"description\",\"Value\":\"40 ml, 15% vol.alc.\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":23},{\"Key\":\"name\",\"Value\":\"Campari\"},{\"Key\":\"type\",\"Value\":\"breci\"},{\"Key\":\"price\",\"Value\":9.9},{\"Key\":\"description\",\"Value\":\"40 ml,25%  vol.alc.\"},{\"Key\":\"gram\",\"Value\":\"\"}],[{\"Key\":\"id\",\"Value\":25},{\"Key\":\"name\",\"Value\":\"Jack Daniels \"},{\"Key\":\"type\",\"Value\":\"breci\"},{\"Key\":\"price\",\"Value\":9.9},{\"Key\":\"description\",\"Value\":\"40 ml, 40% vol.alc.\"},{\"Key\":\"gram\",\"Value\":\"\"}]]}";

/*,[{"Key":"id","Value":26},{"Key":"name","Value":"J&B"},{"Key":"type","Value":"breci"},{"Key":"price","Value":8.9},{"Key":"description","Value":"40 ml, 40% vol.alc. "},{"Key":"gram","Value":""}],[{"Key":"id","Value":27},{"Key":"name","Value":"Jagermeister"},{"Key":"type","Value":"breci"},{"Key":"price","Value":8.9},{"Key":"description","Value":"40 ml, 35% vol.alc. "},{"Key":"gram","Value":""}],[{"Key":"id","Value":28},{"Key":"name","Value":"Nescafé Frappe "},{"Key":"type","Value":"breci"},{"Key":"price","Value":7.5},{"Key":"description","Value":"simplu\/ cu frisca\/ cu menta"},{"Key":"gram","Value":""}],[{"Key":"id","Value":29},{"Key":"name","Value":"Coca-Cola"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":30},{"Key":"name","Value":"Coca-Cola Zero"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":31},{"Key":"name","Value":"Fanta Portocale"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":32},{"Key":"name","Value":"Sprite"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":33},{"Key":"name","Value":"Schweppes Mandarin"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":34},{"Key":"name","Value":"Schweppes Kinley"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":35},{"Key":"name","Value":"Cappy nectar Portocale"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":36},{"Key":"name","Value":"Cappy nectar Piersici"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":37},{"Key":"name","Value":"Santal"},{"Key":"type","Value":"breci"},{"Key":"price","Value":6.5},{"Key":"description","Value":""},{"Key":"gram","Value":"200"}],[{"Key":"id","Value":38},{"Key":"name","Value":"Nestea piersici"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"250"}],[{"Key":"id","Value":39},{"Key":"name","Value":"Fresh portocale\/grapefruit"},{"Key":"type","Value":"breci"},{"Key":"price","Value":12},{"Key":"description","Value":""},{"Key":"gram","Value":"200"}],[{"Key":"id","Value":40},{"Key":"name","Value":"Limonada Classic"},{"Key":"type","Value":"breci"},{"Key":"price","Value":17},{"Key":"description","Value":""},{"Key":"gram","Value":"1000"}],[{"Key":"id","Value":42},{"Key":"name","Value":"Limonada Calssic"},{"Key":"type","Value":"breci"},{"Key":"price","Value":12},{"Key":"description","Value":""},{"Key":"gram","Value":"450"}],[{"Key":"id","Value":43},{"Key":"name","Value":"Apa minerala Dorna"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"500"}],[{"Key":"id","Value":44},{"Key":"name","Value":"Apa plata Izvorul Alb"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.5},{"Key":"description","Value":""},{"Key":"gram","Value":"500"}],[{"Key":"id","Value":45},{"Key":"name","Value":"Tuborg"},{"Key":"type","Value":"breci"},{"Key":"price","Value":6.9},{"Key":"description","Value":"5% vol.alc."},{"Key":"gram","Value":"330"}],[{"Key":"id","Value":46},{"Key":"name","Value":"Ursus Premium"},{"Key":"type","Value":"breci"},{"Key":"price","Value":5.9},{"Key":"description","Value":"5% vol.alc."},{"Key":"gram","Value":"330"}],[{"Key":"id","Value":47},{"Key":"name","Value":"Carlsberg"},{"Key":"type","Value":"breci"},{"Key":"price","Value":7.9},{"Key":"description","Value":"5.4% vol.alc."},{"Key":"gram","Value":"330"}]]}";*/


@implementation DrinkTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
     [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
     [pizzaReq setHTTPMethod:@"GET"];
     
     
     NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
     
     [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
     NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
     
     NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
     self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
     
     */
    self.cnt = 0;
    
    self.data = jsonwebstr;
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];
    
    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"DrinkResult"];
    self.drinkList = [NSMutableArray array];
    self.coldDrinkList = [NSMutableArray array];
  //  [self.drinkList addObject:@"BAUTURI CALDE:"];
    
    for(int i = 0; i < [self.arr count]; i++){
        self.d = [[Drink alloc] init];
        self.cnt++;
        self.d.did = [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"];
        self.d.name = [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"];
        self.d.descr = [[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"];
        self.d.price = [[self.arr objectAtIndex:i] objectAtIndex:3][@"Value"];
        self.d.gr = [[self.arr objectAtIndex:i] objectAtIndex:5][@"Value"];
        
        NSString *drinkType = [[self.arr objectAtIndex:i] objectAtIndex:2][@"Value"];
        
        if([drinkType isEqualToString:@"bcald"]){

            if([[[self.arr objectAtIndex:i] objectAtIndex:5][@"Value"]  isEqual: @""]){
                [self.drinkList addObject:[[[[self.d.name stringByAppendingString:@" "]
                            stringByAppendingString:[NSString stringWithFormat:@"%@", self.d.price]]
                            stringByAppendingString:@" lei \n"]
                            stringByAppendingString:self.d.descr]];
            }
            else
                [self.drinkList addObject:[[[[[[self.d.name stringByAppendingString:@" "]
                    stringByAppendingString:[NSString stringWithFormat:@"%@",self.d.gr ]]
                    stringByAppendingString:@" g "]
                    stringByAppendingString:[NSString stringWithFormat:@"%@", self.d.price]]
                    stringByAppendingString:@" lei \n"]
                    stringByAppendingString:self.d.descr]];
      
        }
        else{
            if([[[self.arr objectAtIndex:i] objectAtIndex:5][@"Value"]  isEqual: @""]){
                [self.coldDrinkList addObject:[[[[self.d.name stringByAppendingString:@" "]
                                             stringByAppendingString:[NSString stringWithFormat:@"%@", self.d.price]]
                                            stringByAppendingString:@" lei \n"]
                                           stringByAppendingString:self.d.descr]];
            }
            else
                [self.coldDrinkList addObject:[[[[[[self.d.name stringByAppendingString:@" "]
                                               stringByAppendingString:[NSString stringWithFormat:@"%@",self.d.gr ]]
                                              stringByAppendingString:@" g "]
                                             stringByAppendingString:[NSString stringWithFormat:@"%@", self.d.price]]
                                            stringByAppendingString:@" lei \n"]
                                           stringByAppendingString:self.d.descr]];

        }
        
        
    //    [self.drinkList addObject: [self.d.name stringByAppendingString:self.d.name]];
    }
    
    [self.drinkList addObject:@""];
  //  [self.drinkList addObject:@"BAUTURI RECI:"];
    
    for(int i=0;i<[self.coldDrinkList count];i++){
        [self.drinkList addObject: [self.coldDrinkList objectAtIndex:i]];
    }

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return self.cnt + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Drinks";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UIColor *color = [UIColor colorWithRed:215/255.0
                                     green:184/255.0
                                      blue:216/255.0
                                     alpha:1];
    
    tableView.backgroundColor = color;
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.text = [self.drinkList objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {

    self.drinkDictionary = [NSDictionary dictionaryWithObject: [self.drinkList objectAtIndex: indexPath.row ] forKey:@1];
    self.drinkIdDictionary = [NSDictionary dictionaryWithObject: @(indexPath.row + 1) forKey:@1];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DrinkAdded" object:self userInfo:self.drinkDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"DrinkAddedId" object:self userInfo:self.drinkIdDictionary];
}

@end
