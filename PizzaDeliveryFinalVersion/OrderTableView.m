//
//  OrderTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "OrderTableView.h"
#import "Order.h"

@interface OrderTableView()

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) NSMutableArray *selItems;
@property (strong, nonatomic) NSMutableArray *selPizzaId;
@property (strong, nonatomic) NSMutableArray *selDrinkId;
@property (strong, nonatomic) NSMutableArray *selSauceId;
@property (strong, nonatomic) NSString *user;
@property (strong, nonatomic) NSNumber *phone;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *orderList;
@property (strong, nonatomic) NSString *location;
@property NSInteger cnt;
@property NSInteger displayedItem;

@end

@implementation OrderTableView

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.tableView reloadData];
    
//[self.selItems addObject:@"Produse adaugate:"];
    
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self.selItems = [NSMutableArray array];
    self.selPizzaId = [NSMutableArray array];
    self.selDrinkId = [NSMutableArray array];
    self.selSauceId = [NSMutableArray array];
    self.cnt = 0;
    self.displayedItem = 0;
    
    self = [super initWithCoder:coder];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:@"PizzaAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sauceEventHandler:) name:@"SauceAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(drinkEventHandler:) name:@"DrinkAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userEventHandler:) name:@"UserAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(phoneEventHandler:) name:@"PhoneAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addrEventHandler:) name:@"AddressAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishEventHandler:) name:@"Finish" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pidEventHandler:) name:@"PizzaAddedId" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEventHandler:) name:@"DrinkAddedId" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sidEventHandler:) name:@"SauceAddedId" object:nil];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self selector:@selector(locationEventHandler:) name:@"LocationAdded" object:nil];
        
    }
    return self;
}

-(void)eventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
   
    [self.selItems addObject:[userInfo objectForKey:@1]];
    [self.tableView reloadData];
}

-(void)pidEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    [self.selPizzaId addObject:[userInfo objectForKey:@1]];

}

-(void)sauceEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    [self.selItems addObject:[userInfo objectForKey:@1]];
    [self.tableView reloadData];
    
}

-(void)drinkEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    [self.selItems addObject: [userInfo objectForKey:@1]];
    [self.tableView reloadData];
    
}
-(void)sidEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    [self.selSauceId addObject:[userInfo objectForKey:@1]];
    
}

-(void)didEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    [self.selDrinkId addObject: [userInfo objectForKey:@1]];
    
}
-(void)userEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.user = [userInfo objectForKey:@1];
    
}
-(void)phoneEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.phone = [userInfo objectForKey:@1];
    
}
-(void)addrEventHandler: (NSNotification*) notification
{
    NSDictionary *userInfo = notification.userInfo;
    self.address = [userInfo objectForKey:@1];
    
}

-(void)locationEventHandler: (NSNotification*) notification
{
    NSDictionary* userInfo = notification.userInfo;
    self.location = [userInfo objectForKey:@1];
    
}

-(void)finishEventHandler: (NSNotification*) notification
{
    
    self.order = [[Order alloc] init];
    self.order.pizzaId = self.selPizzaId;
    self.order.sauceId = self.selSauceId;
    self.order.drinkId = self.selDrinkId;
    self.order.name = self.user;
    self.order.phone = self.phone;
    self.order.address = self.address;
    self.order.location = self.location;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewOrderAdded" object:self];
    
    //TODO: request for adding the new order to database instead of notification
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return [self.selItems count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Order";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }

    UIColor *color = [UIColor colorWithRed:215/255.0
                                     green:184/255.0
                                      blue:216/255.0
                                     alpha:1];
    
    tableView.backgroundColor = color;
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.text = [self.selItems objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    [self.selItems removeObjectAtIndex:indexPath.row];
    
    [tableView reloadData];
    
}


@end
