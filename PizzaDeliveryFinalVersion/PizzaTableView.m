//
//  PizzaTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "PizzaTableView.h"
#import "Pizza.h"

@interface PizzaTableView()

@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;

@property NSInteger cnt;
@property (strong, nonatomic) NSMutableArray *pizzaList;
@property (strong, nonatomic) NSDictionary *pizzaDictionary;
@property (strong, nonatomic) NSDictionary *pizzaIdDictionary;
@property (strong, nonatomic) Pizza *p;

@end

/*
NSString *jsonwebstring = @"{\"PizzaResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";
*/

NSString *jsonwebstring = @"{\"PizzaResult\":[[{\"Key\":\"id\",\"Value\":1},{\"Key\":\"name\",\"Value\":\"Acciughe e capperi\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,mozzarella,acciughe,cappere\"},{\"Key\":\"gram\",\"Value\":620},{\"Key\":\"price\",\"Value\":18.9}],[{\"Key\":\"id\",\"Value\":2},{\"Key\":\"name\",\"Value\":\"Bismark\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,ceapa,ou,mozzarella,condiment\"},{\"Key\":\"gram\",\"Value\":590},{\"Key\":\"price\",\"Value\":17.9}],[{\"Key\":\"id\",\"Value\":3},{\"Key\":\"name\",\"Value\":\"Brezaola rucola e grana\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,brezaola,rucola,grana,mozzarella\"},{\"Key\":\"gram\",\"Value\":680},{\"Key\":\"price\",\"Value\":23.9}],[{\"Key\":\"id\",\"Value\":4},{\"Key\":\"name\",\"Value\":\"Buffolla\"},{\"Key\":\"description\",\"Value\":\"aluat pizza. sos pizza,mozzarella,bufolla,rosii chery,bazzilico frescho\"},{\"Key\":\"gram\",\"Value\":660},{\"Key\":\"price\",\"Value\":21}],[{\"Key\":\"id\",\"Value\":5},{\"Key\":\"name\",\"Value\":\"Calzone\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,ciuperci,carnati de casa,mozzarella,condiment\"},{\"Key\":\"gram\",\"Value\":500},{\"Key\":\"price\",\"Value\":17.9}],[{\"Key\":\"id\",\"Value\":6},{\"Key\":\"name\",\"Value\":\"Campagna\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,bacon,porumb,usturoi,gogosari murati,gorgonzola,mozzarella\"},{\"Key\":\"gram\",\"Value\":740},{\"Key\":\"price\",\"Value\":21}],[{\"Key\":\"id\",\"Value\":7},{\"Key\":\"name\",\"Value\":\"Canibale\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,salam,carnat de casa,mozzareella,condiment\"},{\"Key\":\"gram\",\"Value\":710},{\"Key\":\"price\",\"Value\":18.9}],[{\"Key\":\"id\",\"Value\":8},{\"Key\":\"name\",\"Value\":\"Canibale extra\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,salam,carnati de casa,mozzarella,ciuperci,ceapa,condiment\"},{\"Key\":\"gram\",\"Value\":800},{\"Key\":\"price\",\"Value\":20.9}],[{\"Key\":\"id\",\"Value\":9},{\"Key\":\"name\",\"Value\":\"Canibale suprem\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,salam,carnati,piept de pui,kaiser,mozzarella,condiment\"},{\"Key\":\"gram\",\"Value\":750},{\"Key\":\"price\",\"Value\":20.9}],[{\"Key\":\"id\",\"Value\":10},{\"Key\":\"name\",\"Value\":\"Capriciosa\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,sunca,ton,ardei gras,mozzarella,condiment\"},{\"Key\":\"gram\",\"Value\":690},{\"Key\":\"price\",\"Value\":17.9}],[{\"Key\":\"id\",\"Value\":11},{\"Key\":\"name\",\"Value\":\"Carbonara\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,mozzarella,kaiser,ou,smantana\"},{\"Key\":\"gram\",\"Value\":620},{\"Key\":\"price\",\"Value\":18.9}],[{\"Key\":\"id\",\"Value\":12},{\"Key\":\"name\",\"Value\":\"Castiglione\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,mozzarella,telemea,rosii chery,busuioc verde,oregano\"},{\"Key\":\"gram\",\"Value\":620},{\"Key\":\"price\",\"Value\":19.9}],[{\"Key\":\"id\",\"Value\":13},{\"Key\":\"name\",\"Value\":\"Crazy mix\"},{\"Key\":\"description\",\"Value\":\"aluat pizza,sos pizza,mozzrella,sunca,ardei gras,ceapa rosie,fasole peruana\"},{\"Key\":\"gram\",\"Value\":800},{\"Key\":\"price\",\"Value\":19.9}]]}";

@implementation PizzaTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
    [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
    [pizzaReq setHTTPMethod:@"GET"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
        NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
   
        NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
    self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
    
      */

    self.cnt = 0;

    self.data = jsonwebstring;

    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];

    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"PizzaResult"];    //arr[arr]
    self.pizzaList = [NSMutableArray array];
  //  [self.pizzaList addObject:@"Pizza"];
    
    for(int i = 0; i < [self.arr count]; i++){
        self.p = [[Pizza alloc] init];
        self.cnt++;
        self.p.pid = [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"];
        self.p.name = [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"];
        self.p.descr = [[self.arr objectAtIndex:i] objectAtIndex:2][@"Value"];
        self.p.price = [[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"];
        self.p.gr = [[self.arr objectAtIndex:i] objectAtIndex:3][@"Value"];
        [self.pizzaList addObject:[[[[[[self.p.name stringByAppendingString:@" "]
                                        stringByAppendingString:[NSString stringWithFormat:@"%@", self.p.gr]]
                                       stringByAppendingString:@" g "]
                                      stringByAppendingString:[NSString stringWithFormat:@"%@", self.p.price]]
                                     stringByAppendingString:@" lei \n"]
                                   stringByAppendingString:self.p.descr]];
       

       /*   [self.pizzaList addObject: [self.p.name stringByAppendingString:self.p.name]];*/
        
    }
    
   [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return self.cnt;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pizza list";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UIColor *color = [UIColor colorWithRed:215/255.0
                                     green:184/255.0
                                      blue:216/255.0
                                     alpha:1];
    
    tableView.backgroundColor = color;
    cell.textLabel.textColor = [UIColor blueColor];
    cell.textLabel.text = [self.pizzaList objectAtIndex:indexPath.row];


    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    self.pizzaDictionary = [NSDictionary dictionaryWithObject: [self.pizzaList objectAtIndex: indexPath.row ] forKey:@1];
    self.pizzaIdDictionary = [NSDictionary dictionaryWithObject: @(indexPath.row + 1) forKey:@1];
    
  [[NSNotificationCenter defaultCenter] postNotificationName: @"PizzaAdded" object:self userInfo:self.pizzaDictionary];
  [[NSNotificationCenter defaultCenter] postNotificationName: @"PizzaAddedId" object:self userInfo:self.pizzaIdDictionary];

}

@end
