//
//  RestViewController.h
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKMaps/SKMaps.h"

@import CoreLocation;


@interface RestViewController : UIViewController <SKRoutingDelegate, SKNavigationDelegate, SKMapViewDelegate, SKPositionerServiceDelegate>

@end
