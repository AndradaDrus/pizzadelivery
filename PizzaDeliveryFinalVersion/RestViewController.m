//
//  RestViewController.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "RestViewController.h"
#import "SKMaps/SKMaps.h"
#import "AsyncBlockOperation.h"


@interface RestViewController()

@property CLLocationCoordinate2D clientAddrCoordinate;
@property double l1lat;
@property double l1long;
@property double l2lat;
@property double l2long;
@property double addrLat;
@property double addrLong;
@property double navigateToLat;
@property double navigateToLong;
@property NSString *address;
@property CLLocationCoordinate2D crtLoc;
@property CLGeocoder *geocoder;
@property NSInteger distance1, distance2;
@property NSMutableArray *dist;
@property BOOL chooseLoc;
@property NSOperationQueue *queue;
@property SKRouteSettings *routeSettings;
@property SKRouteSettings *routeSettings2;
@property AsyncBlockOperation *dist1;
@property AsyncBlockOperation *dist2;

@end

@implementation RestViewController

- (void) viewDidLoad{
    [super viewDidLoad];
    self.l1lat = 46.76;     //Republicii
    self.l1long = 23.591423;
    
    self.l2lat = 46.7786;   //Marasti
    self.l2long = 23.6106;
    
    self.addrLat = 46.78;   //client address
    self.addrLong = 23.6106;
    
    self.distance1 = 0;
    self.distance2 = 0;
    self.dist = [NSMutableArray array];
    
    self.chooseLoc = NO;
    
    SKMapView* mapView = [[SKMapView alloc] initWithFrame:CGRectMake( 0.0f, 0.0f, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
     mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
 
    [self.view addSubview:mapView];
   
    [self.view sendSubviewToBack:mapView];

    mapView.settings.showCurrentPosition = YES;
    
    SKMapInternationalizationSettings* internationalizationSettings = [SKMapInternationalizationSettings mapInternationalization];
    internationalizationSettings.primaryOption = SKMapInternationalizationOptionLocal;
    internationalizationSettings.primaryInternationalLanguage = SKMapLanguageLOCAL;
    mapView.settings.mapInternationalization = internationalizationSettings;
    
    mapView.settings.showCompass = YES;
    
    mapView.mapScaleView.hidden = NO;
    
    SKCoordinateRegion region;
    region.zoomLevel = 14;
    region.center = CLLocationCoordinate2DMake(46.770436, 23.60);
    mapView.visibleRegion = region;
    
    //Place retaurants
    SKAnnotation* annotation = [SKAnnotation annotation];
    annotation.identifier = 10;
    annotation.annotationType = SKAnnotationTypePurple;
    annotation.location = CLLocationCoordinate2DMake(self.l1lat, self.l1long);
    SKAnimationSettings* animationSettings = [SKAnimationSettings animationSettings];
    [mapView addAnnotation:annotation withAnimationSettings:animationSettings];
    
    SKAnnotation* annotation2 = [SKAnnotation annotation];
    annotation2.identifier = 12;
    annotation2.annotationType = SKAnnotationTypePurple;
    annotation2.location = CLLocationCoordinate2DMake(self.l2lat, self.l2long);
    [mapView addAnnotation:annotation2 withAnimationSettings:animationSettings];
    
    mapView.delegate = self;
    
    [SKRoutingService sharedInstance].routingDelegate = self;
    [SKRoutingService sharedInstance].navigationDelegate = self;
    [SKRoutingService sharedInstance].mapView = mapView;

    SKPositionerService *positionerService = [[SKPositionerService alloc] init];
    positionerService.delegate = self;
    
    
    self.routeSettings = [[SKRouteSettings alloc]init];
    
    self.routeSettings.startCoordinate = CLLocationCoordinate2DMake(self.addrLat, self.addrLong);
    self.routeSettings.destinationCoordinate = CLLocationCoordinate2DMake(self.l1lat, self.l1long);
    self.routeSettings.shouldBeRendered = NO;
    self.routeSettings.routeMode = SKRouteCarFastest;
    SKRouteRestrictions routeRestr = self.routeSettings.routeRestrictions;
    routeRestr.avoidHighways = YES;
    self.routeSettings.routeRestrictions = routeRestr;

    
    self.routeSettings2 = [[SKRouteSettings alloc]init];
    
    self.routeSettings2.startCoordinate = CLLocationCoordinate2DMake(self.addrLat, self.addrLong);
    self.routeSettings.destinationCoordinate = CLLocationCoordinate2DMake(self.l2lat,self.l2long);
    self.routeSettings2.shouldBeRendered = NO;
    self.routeSettings2.routeMode = SKRouteCarFastest;
    self.routeSettings2.routeRestrictions = routeRestr;

  }
- (IBAction)startNavig:(UIButton *)sender {

    SKRouteSettings* route = [[SKRouteSettings alloc]init];
    route.startCoordinate = CLLocationCoordinate2DMake(self.crtLoc.latitude, self.crtLoc.longitude);
    route.destinationCoordinate = CLLocationCoordinate2DMake(self.navigateToLat, self.navigateToLong);
    
    route.requestAdvices = YES;
    
    route.shouldBeRendered = YES;
    route.routeMode = SKRouteCarFastest;
    route.maximumReturnedRoutes = 1;
    SKRouteRestrictions routeRestrictions = route.routeRestrictions;
    routeRestrictions.avoidHighways = YES;
    route.routeRestrictions = routeRestrictions;
    [[SKRoutingService sharedInstance]calculateRoute:route];
}
- (IBAction)stopNavig:(UIButton *)sender {
     [[SKRoutingService sharedInstance] stopNavigation];
}

- (instancetype)initWithCoder:(NSCoder*)coder{
    self = [super initWithCoder:coder];
    if(self){
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addressEventHandler:) name:@"AddressAdded" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(finishEventHandler:) name:@"Finish" object:nil];
       }
    return self;
}

- (void)positionerService:(SKPositionerService *)positionerService updatedCurrentLocation:(CLLocation *)currentLocation{
    self.crtLoc = positionerService.currentCoordinate;
   NSLog(@"%f %f", self.crtLoc.latitude, self.crtLoc.longitude);

}

- (void)mapView:(SKMapView*)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    self.navigateToLat = coordinate.latitude;
    self.navigateToLong = coordinate.longitude;
    
}

- (void)routingService:(SKRoutingService*)routingService didFinishRouteCalculationWithInfo:(SKRouteInformation*)routeInformation{
    [routingService zoomToRouteWithInsets:UIEdgeInsetsZero duration:1];

    SKNavigationSettings* navSettings = [SKNavigationSettings navigationSettings];
    navSettings.navigationType=SKNavigationTypeSimulation;
    navSettings.distanceFormat=SKDistanceFormatMetric;
    navSettings.showStreetNamePopUpsOnRoute=YES;
    [SKRoutingService sharedInstance].mapView.settings.displayMode = SKMapDisplayMode3D;
    [[SKRoutingService sharedInstance]startNavigationWithSettings:navSettings];
    
}

- (void)routingServiceDidFailRouteCalculation:(SKRoutingService*)routingService{
    NSLog(@"Route calculation failed.");
}

-(void)computeDist1{
    
    [SKRoutingService sharedInstance].routingDelegate = self;
    SKRouteSettings* routeToL1 = [[SKRouteSettings alloc]init];
    
    routeToL1.startCoordinate = CLLocationCoordinate2DMake(self.addrLat, self.addrLong);
    routeToL1.destinationCoordinate = CLLocationCoordinate2DMake(self.l1lat, self.l1long);
    
    routeToL1.shouldBeRendered = NO;
    routeToL1.routeMode = SKRouteCarFastest;
    SKRouteRestrictions routeRestrictions = routeToL1.routeRestrictions;
    routeRestrictions.avoidHighways = YES;
    routeToL1.routeRestrictions = routeRestrictions;
    [[SKRoutingService sharedInstance]calculateRoute:routeToL1];

}

-(void) computeDist2{
    
    [SKRoutingService sharedInstance].routingDelegate = self;
    SKRouteSettings* routeToL2 = [[SKRouteSettings alloc]init];
    
    routeToL2.startCoordinate = CLLocationCoordinate2DMake(self.addrLat, self.addrLong);
    routeToL2.destinationCoordinate = CLLocationCoordinate2DMake(self.l2lat, self.l2long);
    
    routeToL2.shouldBeRendered = NO;
    routeToL2.routeMode = SKRouteCarFastest;
    SKRouteRestrictions routeRestr = routeToL2.routeRestrictions;
    routeRestr.avoidHighways = YES;
    routeToL2.routeRestrictions = routeRestr;
    
    [[SKRoutingService sharedInstance]calculateRoute:routeToL2];
}


-(void)finishEventHandler: (NSNotification*) notification{
    NSDictionary *loc;
    if(self.dist1.dist == nil)
        loc = [NSDictionary dictionaryWithObject:@"L2" forKey:@1];
    
    else
        [self.dist addObject: self.dist1.dist];
    if(self.dist2.dist == nil)
        loc = [NSDictionary dictionaryWithObject:@"L1" forKey:@1];
    else
        [self.dist addObject: self.dist2.dist];

    if([self.dist count]==2){
        if([self.dist objectAtIndex:0] < [self.dist objectAtIndex:1])
            loc = [NSDictionary dictionaryWithObject:@"L1" forKey:@1];
        else
            loc = [NSDictionary dictionaryWithObject:@"L2" forKey:@1];
    }
    NSLog(@"Order sent to location %@",loc);
}

-(void)addressEventHandler: (NSNotification*) notification
{
    NSDictionary* userInfo = notification.userInfo;
    self.address = [userInfo objectForKey:@1];
    
    //user specifies only the street name
    self.address = [self.address stringByAppendingString:@" Cluj"];
    
    self.geocoder = [[CLGeocoder alloc]init];
    [self.geocoder geocodeAddressString: self.address completionHandler:^(NSArray *placemarks, NSError *error){
        if (error)
            NSLog(@"ERROR: %@", error);
        else{
            CLPlacemark *placemark = [placemarks lastObject];
            self.addrLat = placemark.location.coordinate.latitude;
            self.addrLong = placemark.location.coordinate.longitude;
        }
    }];
    
        self.queue = [[NSOperationQueue alloc] init];
        self.queue.maxConcurrentOperationCount = 1;
    
        self.dist1 = [[AsyncBlockOperation alloc] initWithRoute: self.routeSettings];
        [self.queue addOperation:self.dist1];

        self.dist2 = [[AsyncBlockOperation alloc] initWithRoute: self.routeSettings2];
        [self.queue addOperation:self.dist2];
}

@end


