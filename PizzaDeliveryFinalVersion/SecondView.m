//
//  SecondView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "SecondView.h"

@interface SecondView()

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *msg;
@property (strong, nonatomic) NSDictionary *u;
@property (strong, nonatomic) NSDictionary *p;
@property (strong, nonatomic) NSDictionary *a;
@end

@implementation SecondView

@synthesize userName;
@synthesize phone;
@synthesize address;

- (void)viewDidLoad{

}

-(IBAction) btnClicked:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Finish" object:self];
    self.userName.text = @"";
    self.phone.text = @"";
    self.address.text = @"";
    self.msg.text = @"Comanda dumneavoastra a fost adaugata.";
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{

    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.u = [NSDictionary dictionaryWithObject:self.userName.text forKey:@1];
    if([self.phone.text length]==10)
        self.p = [NSDictionary dictionaryWithObject:[f numberFromString:self.phone.text] forKey:@1];
    else {
        self.msg.text = @"Numar incorect!";
        self.phone.text = @"";
    }
    self.a = [NSDictionary dictionaryWithObject:self.address.text forKey:@1];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserAdded" object:self userInfo:self.u];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PhoneAdded" object:self userInfo:self.p];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddressAdded" object:self userInfo:self.a];
    
        return YES;
}

@end