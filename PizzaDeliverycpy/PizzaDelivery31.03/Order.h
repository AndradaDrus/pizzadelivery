//
//  Order.h
//  PizzaDelivery
//
//  Created by user on 31/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject
@property (strong, nonatomic) NSMutableArray *pizzaId;
@property (strong, nonatomic) NSMutableArray *sauceId;
@property (strong, nonatomic) NSMutableArray *drinkId;
@property (strong, nonatomic) NSNumber *clientId;
@property (strong, nonatomic) NSNumber *orderId;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *phone;

@end
