//
//  PizzaTableView.h
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PizzaTableView : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *nameArr;

@end
