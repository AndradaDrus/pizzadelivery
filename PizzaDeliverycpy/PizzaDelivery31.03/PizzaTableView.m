//
//  PizzaTableView.m
//  PizzaDelivery
//
//  Created by user on 25/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import "PizzaTableView.h"
#import "Pizza.h"

@interface PizzaTableView()

@property (strong, nonatomic) NSDictionary *jsonDict;
@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSArray *arr;

@property NSInteger cnt;
@property (strong, nonatomic) NSMutableArray *pizzaList;

@property (strong, nonatomic) Pizza *p;

@end

NSString *jsonwebstring = @"{\"PizzaResult\":[[{\"Key\":\"id\",\"Value\":\"1\"},{\"Key\":\"name\",\"Value\":\"capriciosa\"}],[{\"Key\":\"id\",\"Value\":\"2\"},{\"Key\":\"name\",\"Value\":\"diavola\"}]]}";

@implementation PizzaTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
   /* NSMutableURLRequest *pizzaReq = [[NSMutableURLRequest alloc] init];
    [pizzaReq setURL:[NSURL URLWithString:@"localhost:56545/Server.svc/PizzaMenu"]];
    [pizzaReq setHTTPMethod:@"GET"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:pizzaReq completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
        NSString *reqReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding ];
   
        NSData *jsonData = [reqReply dataUsingEncoding:NSUTF8StringEncoding];
     }] resume];
    self.json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &e];
    
      */
    self.nameArr = [NSMutableArray array];
    
    self.cnt = 0;

    self.data = jsonwebstring;
    
    NSData *reqRepl = [self.data dataUsingEncoding:NSUTF8StringEncoding];

    self.jsonDict = [NSJSONSerialization JSONObjectWithData:reqRepl options:NSJSONReadingMutableContainers error: nil];
    
    self.arr = self.jsonDict[@"PizzaResult"];    //arr[arr]
    self.pizzaList = [NSMutableArray array];
    
    for(int i = 0; i < [self.arr count]; i++){
        self.p = [[Pizza alloc] init];
        self.cnt++;
        self.p.pid = [[self.arr objectAtIndex:i] objectAtIndex:0][@"Value"];
        self.p.name = [[self.arr objectAtIndex:i] objectAtIndex:1][@"Value"];
      /*  self.p.descr = [[self.arr objectAtIndex:i] objectAtIndex:2][@"Value"];
        self.p.price = [[self.arr objectAtIndex:i] objectAtIndex:4][@"Value"];
        self.p.gr = [[self.arr objectAtIndex:i] objectAtIndex:3][@"Value"];
        [self.pizzaList addObject:[[[[[[self.p.name stringByAppendingString:@" "]
                                        stringByAppendingString:self.p.gr]
                                       stringByAppendingString:@" g "]
                                      stringByAppendingString:self.p.price]
                                     stringByAppendingString:@" lei \n"]
                                   stringByAppendingString:self.p.descr]];
       */

          [self.pizzaList addObject: [self.p.name stringByAppendingString:self.p.name]];
    }
    
    
   [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger)section{
    return self.cnt;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pizza list";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.pizzaList objectAtIndex:indexPath.row];


    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    [self.nameArr addObject:[self.pizzaList objectAtIndex:indexPath.row ]];

    NSLog(@"%ld",(long)indexPath.row);

}

@end
