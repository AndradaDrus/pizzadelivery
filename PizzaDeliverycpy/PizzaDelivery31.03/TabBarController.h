//
//  TabBarController.h
//  PizzaDelivery
//
//  Created by user on 31/03/16.
//  Copyright © 2016 superEroul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController
@property (strong, nonatomic) NSMutableArray *pizzaIdList;
@end
